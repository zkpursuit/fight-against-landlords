package ddz.utils;

public class ByteUtils {

    public final static byte[] getBytes(int value) {
        byte[] buf = new byte[4];
        for (int i = buf.length - 1; i >= 0; i--) {
            buf[i] = (byte) (value & 0x000000ff);
            value >>= 8;
        }
        return buf;
    }

    public final static byte[] getBytes(long value) {
        byte[] bytes = new byte[8];
        for (int i = 0; i < 8; i++) {
            int offset = 64 - (i + 1) * 8;
            bytes[i] = (byte) ((value >> offset) & 0xff);
        }
        return bytes;
    }

    public final static byte[] getBytes(boolean value) {
        if (value) return new byte[]{1};
        return new byte[]{0};
    }

    public final static byte[] getBytes(short value) {
        byte[] bytes = new byte[2];
        for (int i = 0; i < 2; i++) {
            int offset = 16 - (i + 1) * 8;
            bytes[i] = (byte) ((value >> offset) & 0xff);
        }
        return bytes;
    }

    public static byte[] getBytes(float value) {
        return getBytes(Float.floatToIntBits(value));
    }

    public static byte[] getBytes(double value) {
        return getBytes(Double.doubleToLongBits(value));
    }

    public final static int toInt(byte[] bytes, int start) {
        if (bytes == null) {
            throw new IllegalArgumentException("byte array is null!");
        }
        int num = 0;
        int last = start + 3;
        for (int i = start; i <= last; i++) {
            num <<= 8;
            num |= (bytes[i] & 0x000000ff);
        }
        return num;
    }

    public final static long toLong(byte[] bytes, int start) {
        if (bytes == null) {
            throw new IllegalArgumentException("byte array is null!");
        }
        long num = 0;
        int end = start + 8;
        for (int i = start; i < end; i++) {
            num <<= 8;
            num |= (bytes[i] & 0xff);
        }
        return num;
    }

    public final static boolean toBoolean(byte[] bytes, int start) {
        if (bytes == null) {
            throw new IllegalArgumentException("byte array is null!");
        }
        byte b = bytes[start];
        if (b == 1) return true;
        return false;
    }

    public final static short toShort(byte[] bytes, int start) {
        if (bytes == null) {
            throw new IllegalArgumentException("byte array is null!");
        }
        short num = 0;
        int last = start + 1;
        for (int i = start; i <= last; i++) {
            num <<= 8;
            num |= (bytes[i] & 0x00ff);
        }
        return num;
    }

    public static float toFloat(byte[] bytes, int start) {
        if (bytes == null) {
            throw new IllegalArgumentException("byte array is null!");
        }
        return Float.intBitsToFloat(toInt(bytes, start));
    }

    public static double toDouble(byte[] bytes, int start) {
        if (bytes == null) {
            throw new IllegalArgumentException("byte array is null!");
        }
        return Double.longBitsToDouble(toLong(bytes, start));
    }


}
