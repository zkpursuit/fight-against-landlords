package ddz.utils;

import com.kaka.util.Charsets;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DES加密介绍 DES是一种对称加密算法，所谓对称加密算法即：加密和解密使用相同密钥的算法。DES加密算法出自IBM的研究，
 * 后来被美国政府正式采用，之后开始广泛流传，但是近些年使用越来越少，因为DES使用56位密钥，以现代计算能力，
 * 24小时内即可被破解。虽然如此，在某些简单应用中，我们还是可以使用DES加密算法，本文简单讲解DES的JAVA实现 。
 * 注意：DES加密和解密过程中，密钥长度都必须是8的倍数。<br>
 * 当默认用DES，JAVA会用ECB模式，因此这里IV向量没有作用，这里，但当用CBC模式下，如果还是用SecureRandom，
 * 则每次加密的结果都会不一样，因为JAVA内部会用随机的IV来初始化Cipher对象，如示例代码，
 * 由于Cipher.getInstance("DES/CBC/PKCS5Padding")使用了CBC，
 * 因此我这里用的javax.crypto.spec.IvParameterSpec包下的IvParameterSpec来初始化向量IV<br>
 * JAVA中默认的算法为ECB，默认填充方式为PKCS5Padding
 */
public class DES {

    private DES() {
    }

    /**
     * 加密
     *
     * @param src byte[] 加密的数据源
     * @param password byte[] 加密秘钥
     * @return byte[] 加密后的数据
     */
    public static byte[] encrypt(byte[] src, byte[] password) {
        try {
            DESKeySpec desKey = new DESKeySpec(password);
            //创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            //ECB模式下，iv不需要
            IvParameterSpec iv = new IvParameterSpec(password);
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, iv);
            //现在，获取数据并加密
            //正式执行加密操作
            return cipher.doFinal(src);
        } catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger(DES.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * 加密
     *
     * @param src byte[] 加密的数据源
     * @param password String 加密秘钥
     * @return byte[] 加密后的数据
     */
    public static byte[] encrypt(byte[] src, String password) {
        byte[] keyBytes = password.getBytes(Charsets.utf8);
        return encrypt(src, keyBytes);
    }

    /**
     * 解密
     *
     * @param src byte[] 解密的数据源
     * @param password byte[] 解密秘钥
     * @return byte[] 解密后的数据
     */
    public static byte[] decrypt(byte[] src, byte[] password) {
        try {
            // DES算法要求有一个可信任的随机数源
            //SecureRandom random = new SecureRandom();
            // 创建一个DESKeySpec对象
            DESKeySpec desKey = new DESKeySpec(password);
            // 创建一个密匙工厂
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            // 将DESKeySpec对象转换成SecretKey对象
            SecretKey securekey = keyFactory.generateSecret(desKey);
            // Cipher对象实际完成解密操作，CBC为加密模式，
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            //ECB模式下，iv不需要
            IvParameterSpec iv = new IvParameterSpec(password);
            // 用密匙初始化Cipher对象
            cipher.init(Cipher.DECRYPT_MODE, securekey, iv);
            // 真正开始解密操作
            return cipher.doFinal(src);
        } catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(DES.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * 解密
     *
     * @param src byte[] 解密的数据源
     * @param password String 解密秘钥
     * @param psdCharset 秘钥字符编码
     * @return byte[] 解密后的数据
     */
    public static byte[] decrypt(byte[] src, String password, Charset psdCharset) {
        byte[] keyBytes = password.getBytes(psdCharset);
        return decrypt(src, keyBytes);
    }

    /**
     * 解密
     *
     * @param src byte[] 解密的数据源
     * @param password String 解密秘钥
     * @return byte[] 解密后的数据
     */
    public static byte[] decrypt(byte[] src, String password) {
        return decrypt(src, password, Charsets.utf8);
    }
}
