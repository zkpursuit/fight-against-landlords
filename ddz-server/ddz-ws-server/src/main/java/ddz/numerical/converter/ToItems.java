package ddz.numerical.converter;

import com.kaka.numerical.annotation.NumericField;
import ddz.numerical.pojo.Item;

/**
 * @author zhoukai
 */
public class ToItems implements NumericField.Converter<Item[]> {

    @Override
    public Item[] transform(String data) {
        if (data == null) {
            return null;
        }
        if ("".equals(data)) {
            return null;
        }
        data = data.trim();
        String[] strs = data.split("[;,，]");
        Item[] array = new Item[strs.length];
        for (int i = 0; i < array.length; i++) {
            String str = strs[i];
            array[i] = ToItem.toItem(str);
        }
        return array;
    }

}
