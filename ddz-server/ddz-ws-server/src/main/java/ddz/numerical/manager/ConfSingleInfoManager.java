package ddz.numerical.manager;

import com.kaka.numerical.annotation.Numeric;
import ddz.numerical.ConfManager;
import ddz.numerical.pojo.ConfSingleInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Numeric(src = "conf_single.txt")
public class ConfSingleInfoManager extends ConfManager<ConfSingleInfo> {

    private final List<ConfSingleInfo> list = Collections.synchronizedList(new ArrayList<>());

    @Override
    public List<ConfSingleInfo> getList() {
        return list;
    }

    @Override
    protected void cacheObject(ConfSingleInfo confSingleInfo) {
        list.add(confSingleInfo);
    }

    @Override
    protected void parseBefore() {
        list.clear();
    }

    @Override
    protected void parseAfter() {
    }

    public ConfSingleInfo getConfSingleInfo() {
        if (list.isEmpty()) return null;
        return list.get(0);
    }
}
