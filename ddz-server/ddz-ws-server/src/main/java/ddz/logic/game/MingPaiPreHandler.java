package ddz.logic.game;

import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.core.TableToken;
import ddz.db.service.TableTokenService;
import ddz.net.EventQueueExecutor;
import ddz.net.LogicDataHandler;
import ddz.net.ProtocolMessage;
import ddz.protos.GameMipai;

@Handler(cmd = OpCode.cmd_mingpai)
public class MingPaiPreHandler extends LogicDataHandler {
    @Override
    protected void execute(ProtocolMessage msg) throws Exception {
        byte[] bytes = (byte[]) msg.getBody();
        GameMipai.CsMing reqData = GameMipai.CsMing.parseFrom(bytes);
        TableTokenService optTokenService = this.retrieveProxy(TableTokenService.class);
        TableToken tokenInfo = optTokenService.parseToken(reqData.getToken());
        long deskId = tokenInfo.getDeskId();
        if (deskId <= 0) return;
        EventQueueExecutor executor = this.retrieveProxy(EventQueueExecutor.class);
        msg.setWhat(OpCode.cmd_mingpai);
        msg.setData(reqData);
        executor.addEvent(String.valueOf(deskId), msg);
    }
}
