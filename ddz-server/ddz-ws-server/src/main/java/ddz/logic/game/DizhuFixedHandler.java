package ddz.logic.game;

import ch.qos.logback.classic.Logger;
import com.kaka.notice.Command;
import com.kaka.notice.Message;
import com.kaka.notice.annotation.Handler;
import com.kaka.util.MathUtils;
import ddz.constants.Crypto;
import ddz.constants.OpCode;
import ddz.core.*;
import ddz.net.WsSender;
import ddz.protos.Game;
import ddz.protos.GameDizhu;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * 确定地主
 *
 * @author zkpursuit
 */
@Handler(cmd = OpCode.cmd_fixed_dizhu, type = String.class)
public class DizhuFixedHandler extends Command implements WsSender {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(DizhuFixedHandler.class);

    @Override
    public void execute(Message message) {
        Table table = (Table) message.getBody();
        if (table == null) return;
        if (table.getState() != TableState.qiang_dizhu) return;
        int dizhuSeatIndex = table.getDizhuSeatIndex();
        Player diZhuPlayer = table.getPlayerBySeatIndex(dizhuSeatIndex);

        int[] diPai = table.getDiCards();
        Cards dizhuHandCards = diZhuPlayer.getMycards();
        dizhuHandCards.addCard(diPai);
        int[] dizhuCards = dizhuHandCards.getCards();
        Game.Cards.Builder diZhuCardsBuilder1 = Game.Cards.newBuilder();
        Game.Cards.Builder diZhuCardsBuilder2 = Game.Cards.newBuilder();
        for (int i = 0; i < dizhuCards.length; i++) {
            diZhuCardsBuilder1.addCard(dizhuCards[i]);
            diZhuCardsBuilder2.addCard(0);
        }
        Game.Cards.Builder diPaiDataBuilder = Game.Cards.newBuilder();
        for (int card : diPai) {
            diPaiDataBuilder.addCard(card);
        }

        boolean isLaiMode = false;
        if (table.getMode() == GameMode.LAIZI.getId()) {
            isLaiMode = true;
        } else if (table.getMode() == GameMode.LIAN_WIN.getId()) {
            if (table.getMode() == GameMode.LAIZI.getId()) {
                isLaiMode = true;
            }
        }
        if (isLaiMode) {
            int laiziPoint = MathUtils.random(3, 14);
            table.setLaiziPoint(laiziPoint);
        }

        boolean dizhuIsMingPai = table.isMingPai(dizhuSeatIndex);

        DiPaiType diPaiType = table.diPaiType(diPai);
        int[] beis = table.getMultiple();

        Player[] players = table.getPlayers();
        for (Player member : players) {
            GameDizhu.ScDingDizhu.Builder builder = GameDizhu.ScDingDizhu.newBuilder();
            builder.setDizhuSeat(dizhuSeatIndex);
            builder.setDiPai(diPaiDataBuilder.build());
            builder.setDiPaiType(diPaiType.getId());
            if (member.getSeatIndex() == dizhuSeatIndex || dizhuIsMingPai) {
                builder.setDizhuCards(diZhuCardsBuilder1.build());
            } else {
                builder.setDizhuCards(diZhuCardsBuilder2.build());
            }
            if (table.getLaiziPoint() > 0) {
                builder.setLaizi(table.getLaiziPoint());
            }
            builder.setBei(beis[member.getSeatIndex()]);
            this.sendData(member.getUid(), Crypto.isCrypto, Integer.parseInt(OpCode.cmd_fixed_dizhu), builder.build().toByteArray());
        }

        if (logger.isInfoEnabled()) {
            logger.info("房间（{}）确定地主（{} : {}）,地主获得的牌：{}", table.getId(), dizhuSeatIndex, diZhuPlayer.getUid(), Arrays.toString(diPai));
        }

        if (table.getMode() == GameMode.LIAN_WIN.getId()) {
            table.setState(TableState.playing);
        } else {
            table.setState(TableState.jia_bei);
        }
        sendMessage(new Message(OpCode.cmd_show_opts, table));
    }
}
