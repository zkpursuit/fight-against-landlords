package ddz.logic;

import com.kaka.notice.Command;
import com.kaka.notice.Message;
import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.db.service.MatcherService;

/**
 * 玩家匹配
 *
 * @author zkpursuit
 */
@Handler(cmd = OpCode.timer_player_match, type = String.class)
public class TimerMatchHandler extends Command {
    @Override
    public void execute(Message message) {
        MatcherService matcher3Service = this.retrieveProxy(MatcherService.class);
        matcher3Service.matchTick();
    }
}
