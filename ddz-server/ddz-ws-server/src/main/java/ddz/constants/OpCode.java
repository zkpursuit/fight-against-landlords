package ddz.constants;

/**
 * @author zkpursuit
 */
public class OpCode {

    public final static String close_channel = "close_channel";

    public final static int core_cmd = 0xFFFFFF;

    public final static String cmd_heart_beat = "99";

    public final static int cmd_error = 777;           //错误码或信息
    public final static String cmd_rebind = "999";      //重连
    public final static String cmd_login = "1000";    //玩家绑定socket信道
    public final static String cmd_create_desk = "1001";    //创建牌桌
    public final static String cmd_player_list = "1002";      //推送玩家信息列表
    public final static String cmd_ready = "1003";          //准备
    public final static String cmd_fapai = "1004";     //发牌
    public final static String cmd_qiangdizhu = "1005";     //抢地主 
    public final static String cmd_chu_card = "1006";       //玩家出牌
    public final static String cmd_pass = "1007";           //过牌 
    public final static String cmd_show_opts = "1008";   //显示玩家操作按钮
    public final static String cmd_fixed_dizhu = "1009";    //确定地主
    public final static String cmd_jiabei = "1010";     //加倍
    public final static String cmd_mingpai = "1011"; //明牌
    public final static String cmd_counter = "1012"; //记牌器
    public final static String cmd_select_place = "1013"; //选择场次
    public final static String cmd_select_mode = "1014"; //选择玩法模式

    public final static String cmd_jiesuan = "1015";          //结算
    public final static String cmd_trusteeship = "1016";        //托管
    public final static String cmd_recovery = "1017";       //数据恢复
    public final static String cmd_exit_room = "1018";      //退出房间

    public final static String cmd_lian_win_jiesuan = "1019"; //连胜结算
    public final static String cmd_lian_win_revive = "1020"; //连胜复活
    public final static String cmd_lian_win_keep = "1021"; //连胜结果处理，继续还是领奖退出
    public final static String cmd_benefit = "1022"; //救济金可领取状态推送
    public final static String cmd_benefit_get = "1023"; //领取救助金

    public final static String cmd_email_list = "1030"; //获取邮件列表
    public final static String cmd_email_read = "1031"; //读邮件
    public final static String cmd_email_receive_annex = "1032"; //领取邮件附件奖励
    public final static String cmd_email_del = "1033"; //删除邮件
    public final static String add_email = "add_email"; //添加邮件

    public final static String cmd_shop_buy = "1040"; //商城购买
    public final static String cmd_shop_buy_log = "1041"; //获取商城购买记录
    public final static String cmd_recharge_buy = "1042"; //充值购买

    public final static String cmd_item_list = "1050"; //获取道具列表
    public final static String cmd_open_red_bag = "1052"; //开启红包
    public final static String cmd_use_props = "1053"; //使用记牌器或优先看底牌
    public final static String cmd_update_item_amount = "1054"; //更新物品数量

    public final static String cmd_chat = "1060"; //游戏内聊天

    public final static String cmd_rank = "1070"; //排行

    public final static String cmd_dailySign = "1080"; //七日签到
    public final static String cmd_dailySign_data = "1081"; //获取七日签到数据

    public final static String cmd_welfare = "1090"; //每日福利

    public final static String init_user_task = "init_user_task"; //初始化玩家任务
    public final static String update_play_task = "update_play_count_task"; //更新对局任务
    public final static String cmd_get_online_list = "1100"; //获取在线时长任务列表
    public final static String cmd_task_list = "1102"; //获取日常任务列表
    public final static String cmd_task_receive_awards = "1103"; //领取日常任务奖励

    public final static String invite_friend = "invite_friend"; //邀请好友公众号服务器请求指令
    public final static String cmd_friend_login = "1110"; //好友首次登录游戏通知
    public final static String cmd_get_friend_list = "1111"; //获取好友列表
    public final static String cmd_get_invite_awards = "1112"; //获取邀请满十人奖励
    public final static String cmd_get_invitee_play_count_awards = "1113"; //获得被邀请者对局次数奖励
    public final static String cmd_give_to_friend = "1114"; //给好友赠送游戏豆

    public final static String cmd_notice = "1200"; //公告
    public final static String send_notice = "send_notice"; //发送公告

    public final static String timer_writeto_db = "timer_writeto_db";
    public final static String timer_ready = "timer_ready";
    public final static String timer_fapai = "timer_fapai";
    public final static String timer_auto_play = "timer_auto_play";
    public final static String timer_player_match = "timer_player_match";
    public final static String timer_init_recharge_rank = "timer_init_recharge_rank";
    public final static String timer_day_rank_rewards = "timer_day_rank_rewards";
    public final static String timer_mon_rank_rewards = "timer_mon_rank_rewards";

    public final static String cmd_test_game = "cmd_test_game";
    public final static String del_user_item = "del_user_item";
    public final static String add_user_item = "add_user_item";
    public final static String get_online_list = "get_online_list";

    public final static String cmd_http_fapai = "cmd_http_fapai";

}
