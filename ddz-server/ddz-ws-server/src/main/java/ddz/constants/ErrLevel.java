package ddz.constants;

/**
 * 错误级别
 * 
 * @author zkpursuit
 */
public enum ErrLevel {

    INFO(1), WARN(2), ERROR(3);

    private final int value;

    private ErrLevel(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

}
