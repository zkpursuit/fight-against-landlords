package ddz.db.entity;

import java.io.Serializable;

abstract public class BaseTable implements Serializable {

    protected Long id; //房间ID
    protected int mode;     //玩法模式
    protected int place;    //玩法模式下对应的游戏场
    protected int maxPlayers;   //最大玩家数量
    protected int baseMultiple; //基础倍率
    protected int bankerSeatIndex; //假定的庄家，其实是每局第一个要地主的玩家座位索引
    protected long createTimestamp; //房间创建的时间戳
    protected long fapaiStartTime; //发牌开始时间
    protected int laiziPoint = -1; //癞子牌点
    protected volatile int step = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getBaseMultiple() {
        return baseMultiple;
    }

    public void setBaseMultiple(int baseMultiple) {
        this.baseMultiple = baseMultiple;
    }

    public int getBankerSeatIndex() {
        return bankerSeatIndex;
    }

    public void setBankerSeatIndex(int bankerSeatIndex) {
        this.bankerSeatIndex = bankerSeatIndex;
    }

    public long getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(long createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public long getFapaiStartTime() {
        return fapaiStartTime;
    }

    public void setFapaiStartTime(long fapaiStartTime) {
        this.fapaiStartTime = fapaiStartTime;
    }

    public int getLaiziPoint() {
        return laiziPoint;
    }

    public void setLaiziPoint(int laiziPoint) {
        this.laiziPoint = laiziPoint;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}
