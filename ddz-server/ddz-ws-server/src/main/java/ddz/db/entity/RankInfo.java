package ddz.db.entity;

/**
 * 排行信息
 *
 * @author zkpursuit
 */
public class RankInfo {
    private long uid;
    private String nickname;
    private String headImg;
    private transient float score;
    private transient int rank;

    public RankInfo() {
    }

    public RankInfo(long uid, float score) {
        this.uid = uid;
        this.score = score;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
