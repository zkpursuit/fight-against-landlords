package ddz.db.mapper;

import ddz.db.entity.TableInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TableSqlMapper {

    TableInfo getDeskInfoById(@Param("id") long id);

    int insertDeskInfo(TableInfo desk);

    void batchInsertDeskInfo(@Param("list") List<TableInfo> list);

    int updateDeskInfo(TableInfo desk);

}
