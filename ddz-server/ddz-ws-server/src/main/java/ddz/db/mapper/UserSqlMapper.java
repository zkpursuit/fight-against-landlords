package ddz.db.mapper;

import ddz.db.entity.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserSqlMapper {

    int existsUser(@Param("uid") Long uid);

    UserInfo getUserInfoByUserId(@Param("uid") Long uid);

    List<UserInfo> getUserInfoByUids(@Param("list") List<Long> list);

    UserInfo getUserInfoByUnionid(@Param("unionid") String unionid);

    String getOpenIdByUid(@Param("uid") Long uid);

    List<UserInfo> getWinRankList();

    List<Long> getRobotUidList();

    void batchInsertUserInfo(@Param("list") List<UserInfo> list);

    List<UserInfo> getOnlyItemsInfoList();

    void updateUserItems(UserInfo info);

}
