package ddz.db.service;

import java.util.List;

public interface MatchTimeoutProcessor {

    /**
     * 匹配超时时间
     *
     * @return 超时时间
     */
    long timeout(MatchPool matchPool);

    /**
     * 超时后的处理
     */
    void doMatchTimeout(MatchPool matchPool, List<MatchMember> members);
}
