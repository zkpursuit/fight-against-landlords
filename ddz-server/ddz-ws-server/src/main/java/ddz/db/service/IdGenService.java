package ddz.db.service;

import com.kaka.notice.Proxy;
import com.kaka.notice.annotation.Model;
import com.kaka.util.SnowFlake;
import ddz.utils.Toolkit;

/**
 * ID生成服务
 *
 * @author zkpursuit
 */
@Model
public class IdGenService extends Proxy {

    private final SnowFlake snowFlake;

    public IdGenService() {
        snowFlake = new SnowFlake(1L, 1L);
    }

    public long nextId() {
        return snowFlake.nextId();
    }

}
