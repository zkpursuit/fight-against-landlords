package ddz.db.service;

public class MatchResult {
    public final MatchMember[] matchMembers;
    public final int mode;
    public final int place;

    public MatchResult(String matcherId, MatchMember[] matchMembers) {
        this.matchMembers = matchMembers;
        String[] parts = matcherId.split(":");
        this.mode = Integer.parseInt(parts[0]);
        this.place = Integer.parseInt(parts[1]);
    }

    public MatchResult(int mode, int place, MatchMember[] matchMembers) {
        this.mode = mode;
        this.place = place;
        this.matchMembers = matchMembers;
    }
}
