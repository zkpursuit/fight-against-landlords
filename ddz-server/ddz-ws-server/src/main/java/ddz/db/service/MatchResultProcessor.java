package ddz.db.service;

/**
 * 匹配结果处理器
 */
public interface MatchResultProcessor {
    /**
     * 处理匹配结果
     *
     * @param result
     */
    void doMatchResult(MatchPool matchPool, MatchResult result);
}
