package ddz.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 扑克牌花色
 */
public enum SuitType {

    heitao(4, "黑桃"),
    hongxin(3, "红心"),
    meihua(2, "梅花"),
    fangkuai(1, "方块"),
    xiaowang(5, "小王"),
    dawang(6, "大王");

    private final int id;
    private final String name;

    SuitType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * 获取类型值
     *
     * @return 类型值
     */
    public final int getId() {
        return id;
    }

    /**
     * 获取类型名
     *
     * @return 类型名
     */
    public final String getName() {
        return name;
    }

    private final static Map<Integer, SuitType> typeMap = new ConcurrentHashMap<>();

    /**
     * 获取花色类型
     *
     * @param suit 花色
     * @return 花色类型
     */
    public final static SuitType getSuitType(int suit) {
        if (typeMap.isEmpty()) {
            SuitType[] types = SuitType.values();
            for (SuitType type : types) {
                typeMap.put(type.getId(), type);
            }
        }
        if (!typeMap.containsKey(suit)) {
            return null;
        }
        return typeMap.get(suit);
    }

    /**
     * 获取花色名
     *
     * @param suit 花色
     * @return 花色名
     */
    public final static String getSuitName(int suit) {
        SuitType type = getSuitType(suit);
        if (type == null) {
            return null;
        }
        return type.getName();
    }

}
