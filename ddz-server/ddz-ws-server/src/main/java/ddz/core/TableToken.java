package ddz.core;

public class TableToken {

    private long deskId;
    private int step;
    private int nextOperateSeatIndex;

    public TableToken() {
    }

    public TableToken(long deskId, int step, int nextOperateSeatIndex) {
        this.deskId = deskId;
        this.step = step;
        this.nextOperateSeatIndex = nextOperateSeatIndex;
    }

    public long getDeskId() {
        return deskId;
    }

    public void setDeskId(long deskId) {
        this.deskId = deskId;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getNextOperateSeatIndex() {
        return nextOperateSeatIndex;
    }

    public void setNextOperateSeatIndex(int nextOperateSeatIndex) {
        this.nextOperateSeatIndex = nextOperateSeatIndex;
    }
}
