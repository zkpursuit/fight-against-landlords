package ddz.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 玩牌操作类型枚举
 *
 * @author zkpursuit
 */
public enum OptType {
    mingpai(9), // 明牌
    jiaodizhu(10), //叫地主
    bujiaodizhu(11), //不叫
    qiangdizhu(12), //抢地主
    buqiangdizhu(13), //不抢地主
    buJiabei(20), //不加倍
    jiabei1(21), //加一倍
    jiabei2(22), //加二倍
    jiabei3(23), //加三倍
    pass(30), //过（不要）
    chu(31); //出牌

    private final int id;

    /**
     * 构造方法
     *
     * @param id 操作优先级
     */
    OptType(int id) {
        this.id = id;
    }

    /**
     * 获取操作类型的优先级
     *
     * @return 操作类型的优先级
     */
    public int getId() {
        return this.id;
    }

    private static Map<Integer, OptType> map = new ConcurrentHashMap<>();

    static {
        OptType[] vals = OptType.values();
        for (OptType type : vals) {
            map.put(type.getId(), type);
        }
    }

    public static OptType getType(int id) {
        return map.get(id);
    }

}
