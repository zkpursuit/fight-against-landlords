package ddz.core.ai;

import ddz.core.Pile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 关系信息
 */
class RelateInfo {
    public final List<Pile> unrelates; //无关联的牌簇集合
    public final Set<WeightType> types; //仅存储 zha, dan_shun, dui_shun, san_shun, dui, san

    RelateInfo(int pileTotals) {
        this.unrelates = new ArrayList<>();
        this.types = new HashSet<>(6);
    }
}
