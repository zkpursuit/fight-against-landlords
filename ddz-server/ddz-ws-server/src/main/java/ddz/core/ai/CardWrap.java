package ddz.core.ai;

import java.util.List;

/**
 * 附带的单牌或对牌的包装类
 */
class CardWrap {

    int[] cards; //包裹相同牌点的牌数据
    int weight = 0; //权重
    boolean hasLaizi = false; //是否包含癞子

    CardWrap(int[] cards) {
        this.cards = cards;
    }

    static List<CardWrap> sortCardWrapList(List<CardWrap> list) {
        if (list.size() >= 2) {
            list.sort((CardWrap wrap1, CardWrap wrap2) -> {
                if (wrap1.weight > wrap2.weight) return 1;
                if (wrap1.weight < wrap2.weight) return -1;
                return 0;
            });
        }
        return list;
    }

}
