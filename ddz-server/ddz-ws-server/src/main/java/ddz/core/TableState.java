package ddz.core;

/**
 * @author zkpursuit
 */
public enum TableState {

    none(0), //无状态
    ready(1), //准备中
    fa_pai(2), //发牌中
    qiang_dizhu(3), //定地主
    jia_bei(4), //加倍
    playing(5), //对局中
    jiesuan(6),     //结算中
    scrap(7);   //已解散

    private final int value;

    TableState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
