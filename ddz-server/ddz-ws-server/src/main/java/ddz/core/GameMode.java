package ddz.core;

/**
 * 游戏模式
 */
public enum GameMode {

    NORMAL(1, "经典"),
    NO_SHUFFLE(2, "不洗牌"),
    LAIZI(3, "癞子"),
    LIAN_ZHA(4, "无限火力"),
    LIAN_WIN(5, "连胜赛");

    private final int id;
    private final String name;

    GameMode(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static GameMode getModeById(int id) {
        GameMode[] modes = GameMode.values();
        for (GameMode mode : modes) {
            if (mode.id == id) {
                return mode;
            }
        }
        return null;
    }

    public static boolean isExists(int modeId) {
        GameMode[] modes = GameMode.values();
        for (GameMode mode : modes) {
            if (mode.id == modeId) {
                return true;
            }
        }
        return false;
    }

}
