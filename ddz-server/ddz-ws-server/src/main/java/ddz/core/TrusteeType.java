package ddz.core;

/**
 * 托管类型
 *
 * @author zkpursuit
 */
public enum TrusteeType {
    none(0, 0),
    system_auto(1, 2), //超时由系统自动托管
    robot(2, Short.MAX_VALUE), //机器人
    custom(3, Integer.MAX_VALUE), //用户操作
    exit(4, 999); //用户退出后的托管

    private final int id;
    private final int timeoutCount;

    TrusteeType(int id, int timeoutCount) {
        this.id = id;
        this.timeoutCount = timeoutCount;
    }

    public int getId() {
        return id;
    }

    public int getTimeoutCount() {
        return timeoutCount;
    }
}
