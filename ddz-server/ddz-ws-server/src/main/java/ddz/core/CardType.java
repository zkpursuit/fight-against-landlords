package ddz.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 牌型
 *
 * @author zkpursuit
 */
public enum CardType {
    none(0), //无类型
    dan(1), //单张
    dui(2), //一对
    san(3), //三张
    zha(4), //炸弹
    san_dan(5), //三带一张
    san_dui(6), //三带一对
    dan_shun(7), //顺子
    dui_shun(8), //连对
    san_shun(9), //飞机
    feiji_dan(10), //飞机带单牌翅膀
    feiji_dui(11), //飞机带对牌翅膀
    si_dan(12), //四带两单
    si_dui(13), //四带两对
    lianzha(14); //连炸

    private final int id;

    CardType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    private static Map<Integer, CardType> map = new ConcurrentHashMap<>();

    static {
        CardType[] vals = CardType.values();
        for (CardType type : vals) {
            map.put(type.getId(), type);
        }
    }

    public static CardType getType(int id) {
        return map.get(id);
    }
}
