package ddz.core;

public enum DiPaiType {
    NONE(0, 1), //无类型
    HAS_JOKER(1, 2), //包含一王
    TWO_JOKER(2, 4), //包含双王
    HAS_DUI(3, 2), //包含一对
    HAS_SAN(4, 3), //包含三张
    IS_SHUN(5, 4), //连续的
    SAME_SUIT(6, 2), //同花
    SAME_SUIT_SHUN(7, 6); //同花顺

    final int id;
    final int bei;

    DiPaiType(int id, int bei) {
        this.id = id;
        this.bei = bei;
    }

    public int getId() {
        return this.id;
    }

    public int getBei() {
        return this.bei;
    }
}
