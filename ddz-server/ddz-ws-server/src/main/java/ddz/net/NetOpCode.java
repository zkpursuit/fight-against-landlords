package ddz.net;

public class NetOpCode {

    /**
     * msg.getBody() == [ChannelHandlerContext, Throwable]
     */
    public static final String CHANNEL_EXCEPTION = "channel_execption";
    /**
     * msg.getBody() == ChannelHandlerContext
     */
    public static final String CHANNEL_ACTIVE = "channel_active";
    /**
     * msg.getBody() == ChannelHandlerContext
     */
    public static final String CHANNEL_REMOVE = "channel_remove";

    /**
     * msg.getBody() == ChannelHandlerContext
     */
    public static final String READER_IDLE = "reader_idle";
    /**
     * msg.getBody() == ChannelHandlerContext
     */
    public static final String WRITER_IDLE = "writer_idle";
    /**
     * msg.getBody() == ChannelHandlerContext
     */
    public static final String ALL_IDLE = "all_idle";

}
