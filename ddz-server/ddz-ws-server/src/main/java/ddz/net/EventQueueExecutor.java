package ddz.net;

import com.kaka.notice.Message;
import com.kaka.notice.Proxy;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public class EventQueueExecutor extends Proxy {

    private final static Map<String, EventQueue> map = new ConcurrentHashMap<>();

    public void addEvent(String queueId, Message event) {
        EventQueue eventQueue = map.computeIfAbsent(queueId, k -> new EventQueue(this.getFacade()));
        eventQueue.addEvent(event);
    }

}
