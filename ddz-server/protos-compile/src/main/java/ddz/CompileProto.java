package ddz;

import com.kaka.util.ResourceUtils;

import java.io.*;
import java.util.Properties;

public class CompileProto {

    public static void main(String[] args) {
        String projectPath = ResourceUtils.getProjectDirectroyPath();
        projectPath = projectPath.replaceAll("\\\\", "/");
        String javaPath = projectPath + "/ddz-ws-server/src/main/java";
        String resPath = projectPath + "/protos-compile/src/main/resources";
        String protosDir = resPath + "/protos";
        try {
            File dir = new File(protosDir);
            StringBuilder exeFilePath = new StringBuilder();
            StringBuilder modifiedTimeFilePath = new StringBuilder();
            if (dir.isDirectory()) {
                File[] files = dir.listFiles((File _dir, String _name) -> {
                    if (_name.endsWith(".exe")) {
                        exeFilePath.append(_dir.getPath() + "/" + _name);
                        return false;
                    } else if (_name.endsWith(".properties")) {
                        modifiedTimeFilePath.append(_dir.getPath() + "/" + _name);
                        return false;
                    }
                    return true;
                });
                Properties modifiedTimePropes = new Properties();
                modifiedTimePropes.load(new FileInputStream(modifiedTimeFilePath.toString()));
                boolean modifiedTimePropesChanged = false;

                Runtime run = Runtime.getRuntime();
                String exeFilePathStr = exeFilePath.toString();
                exeFilePathStr = exeFilePathStr.replaceAll("\\\\", "/");
                String java_cmd = exeFilePathStr + " -I=" + protosDir + " --java_out=" + javaPath + " ";
                //String js_cmd = exeFilePathStr + " -I=" + protosDir + " --js_out=import_style=commonjs,binary:. ";
                for (File f : files) {
                    boolean compile = true;
                    long lastModifTime = f.lastModified();
                    String fname = f.getName();
                    String key = fname;
                    String storeLastModifTimeStr = modifiedTimePropes.getProperty(key);
                    if (storeLastModifTimeStr != null) {
                        long storeLastModifTime = Long.parseLong(storeLastModifTimeStr);
                        if (lastModifTime == storeLastModifTime) {
                            compile = false;
                        }
                    }
                    if (compile) {
                        modifiedTimePropes.put(key, String.valueOf(lastModifTime));
                        modifiedTimePropesChanged = true;
                        String command = java_cmd + fname;
                        run.exec(command);
                        //run.exec(js_cmd + f.getName());
                        System.out.println("已编译文件：" + fname);
                    }
                }
                if (modifiedTimePropesChanged) {
                    Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(modifiedTimeFilePath.toString(), false), "UTF-8"));
                    modifiedTimePropes.store(writer, "最后修改时间");
                    writer.close();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void copyFile(File src, String destPath) throws IOException {
        InputStream inStream = new FileInputStream(src); //读入原文件
        OutputStream outputStream = new FileOutputStream(destPath, false);
        byte[] buffer = new byte[1024];
        int byteread;
        while ((byteread = inStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, byteread);
        }
        inStream.close();
        Writer writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
        writer.flush();
        writer.close();
        outputStream.close();
    }

}
